import unittest
from my_sum import sum
from fractions import Fraction


class TestSum(unittest.TestCase):
    """
    This test the sum function in my_sum module
    """

    def test_list_int(self):
        """
        Test that it can sum a list of integers
        """
        data = [1, 2, 3]
        result = sum(data)
        self.assertEqual(result, 6, "Should be 6")
    
    def test_list_float(self):
        """
        Test that it can sum list of floats
        """
        data = [.5, 0.232, 1.53244]
        result = sum(data)
        self.assertAlmostEqual(result, 2.26444, places=5, msg="Should be around 2.26444")
    
    def test_list_fractions(self):
        """
        Test that it can sum list of Fractions
        """
        data = [Fraction(2, 5), Fraction(3, 8), Fraction(5, 2), Fraction(10, 11)]
        result = sum(data)
        self.assertEqual(result, Fraction(1841, 440))
    
    def test_bad_list_string(self):
        """
        Test that when a list of strings is passed, TypeError is raised
        """
        data = ["1", "2", "3"]
        with self.assertRaises(TypeError):
            sum(data)
    
    def test_bad_single_value(self):
        """
        Test that passing a single integer raises TypeError instead of returning a value
        """
        data = 1
        with self.assertRaises(TypeError):
            sum(data)
    
    def test_bad_string(self):
        """
        Test that passing a single string raises TypeError
        """
        data = "some string"
        with self.assertRaises(TypeError):
            sum(data)


if __name__ == "__main__":
    unittest.main()
