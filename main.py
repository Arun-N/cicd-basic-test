from my_sum import sum
from fractions import Fraction

print(sum(['a', 'b', 'c']))
print(sum((5, 10, 15, 20)))
print(sum(Fraction(1, 2), Fraction(1, 2), Fraction(3, 5)))